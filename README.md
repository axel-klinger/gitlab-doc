# Einführung in GitLab

Hier ein wenig Text.

```mermaid
graph TD;
    A-->B;
    A-->C;
    B-->D;
    C-->D;
```
    

```dot
digraph {
  Paper [fillcolor = red]
  Data -> Paper
  Software -> Data
  Software -> Paper [color=red, arrowhead = diamond]
}
```


... und hier Video:

<iframe width="560" height="315" scrolling="no" src="//av.tib.eu/player/32414" frameborder="0" allowfullscreen></iframe>

![Test](https://doi.org/10.5446/32414#t=25:29,27:00)

[Test It](https://doi.org/10.5446/32414#t=25:29,27:00)

CC-BY 3.0 Free and Open Source software Conference (FrOSCon) e.V.


[![alt text gitlab intro](http://img.youtube.com/vi/fbZOii_l7M4/0.jpg)](http://www.youtube.com/watch?v=fbZOii_l7M4 "Gitlab Intro")